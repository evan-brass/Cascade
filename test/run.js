import './model.unit.js';
import './view.unit.js';
import './common.unit.js';

mocha.checkLeaks();
mocha.run();